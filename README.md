# FomoDemo1 - Welcome 

Welcome to FomoDemo1 - updated on 1-30-2020

FomoDemo1 is simply the name of a project:
1) To experiment with the FoundMotion Alexa skill, 
2) Brainstorm about potential features,
3) Add new features.

We will most likely create new projects at some point to keep track of various activities.

## FomoDemo1 is a project under development by FoundMotion, Inc.

We will use this GitLab project to keep track of all the project
artifacts: This includes documents, whiteboard pics, bugs, questions, and the like.

## Terminology and Methodogy 

We won't be getting hung up on terminology, but we'll be using common AGILE terms and methods.

The following link provides a high level description of how the Agile methodoly maps into
Gitlab.

https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/

## Getting Started

Go to the following link to get started.

https://gitlab.com/KentL/fomodemo1/blob/master/Getting%20Started.md








