# Getting started with FomoDemo1

As stated in the README file, the initial goal of FomoDemo1 is as 
follows:

FomoDemo1 is simply the name of a project:

1) To experiment with the FoundMotion Alexa skill,
2) Brainstorm about potential features,
3) Add new features.


There, here are the basic steps and instructions for Getting Started.


## Alexa Device Required

You will need an Alexa device of some sort and an Amazon account.
An Amazon Prime is NOT required.

If you don't have an Amazon Echo or some sort of Alexa device,
no problem. You can download the "Amazon Alex App" on your smartphone.

Follow the instructions provided by Amazon for getting your Amazon Alex app set up on your
phone or getting whatever Alexa device you have set up.

KEEP TRACK of the the email address you used to register/set up for Alex App or device.
you need the email address later.

Once you can do standard Alex stuff, you will be ready for the next Step.

*** IMPORTANT ***
You need a working Alex device or Alex Phone App before proceeding.
Make sure you can interact with Alex by performing some of the 
Basic Alex functions

## Getting access to the Found Motion Alex App (skill)

After you are comfortable interacting with Alex,
the next step is to get access to the BETA version
of the FoundMotion Alex Skill.

### Steps for accessing the BETA FoundMotion Skill

1) Email kent.landholm@gmail.com from the email address you used to set up your Alex Device or Alex App.
Ask me to send you the link to access the FoundMotion BETA skill

2) When you get the link..... click on it and follow the instructions.

Here is a link to a document with notes for adding the Alexa Skill

https://gitlab.com/KentL/fomodemo1/blob/master/Found_motion_feedback.pdf










